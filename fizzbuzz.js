function fizzbuzz(start, end) {
    for(let i = start; i <= end; i++) {
        let text = '';
        if(i % 3 === 0) text += 'Fizz';
        if(i % 5 === 0) text += 'Buzz';
        console.log(text || i);
    }
}
